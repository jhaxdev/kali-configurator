# Initialization script to ensure system is up to date and ansible is installed

echo "configuring git"
git config --global user.email "jhaxllc@gmail.com"
git config --global user.name "jhax"
echo "updating repo sources"
sudo apt update > /dev/null 2>&1
echo "initiating full upgrade"
sudo apt full-upgrade -y > /dev/null 2>&1
echo "system upgrade completed, everything current..."

echo "installing ansible"
sudo apt install ansible -y > /dev/null 2>&1
echo "ansible installed"

echo "script exiting"

exit 0
