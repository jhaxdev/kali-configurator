# Kali Configurator

## Pre-reqs
- VMWare virtualization, either fusion, workstation, or player
- Kali Linux, running in aforementioned VM
    - current release as of this writing: 2022.2
    - please note kali is a rolling release and will frequently update, I will try to stay on top of it

## Instructions

1) Clone Repo
```bash
git clone git@gitlab.com:jhaxdev/kali-configurator
cd kali-configurator
```

2) Run initialization script
```bash
chmod 755 init.sh
./init.sh
```
3) Move to Ansible directory and run playbook
```bash
cd ansible
ansible-playbook site.yml
```


